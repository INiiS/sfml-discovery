# SFML Discovery

A small project to discover SFML & rediscover C++


## Requirements

SFML installed (see [SFML official tips](https://www.sfml-dev.org/tutorials/2.5/#getting-started) for installation steps.)  
CMake version >= 3.0  
G++  

## How to build

If using CLion of Jetbrains : just build as you would any project.
  
From CLI : `cmake <path/to/sfml-discovery>` will build the project on path  
From then : `make` to generate the binary sfml-discovery  
Then `./sfml-discovery` to launch the application.

## Author
Sébastien Sattler
